const express = require('express')
const morgan = require('morgan')
require('./db/mongoose')

const userRouter = require('./routes/userRouter')

const app = express()

app.use(morgan('dev'))
app.use(express.json())

app.use('/api/v1/users', userRouter)

module.exports = app