const User = require('./../models/userModel')

exports.getAllUsers = async (req, res) => {
    try{
        let users;
        
        if(req.query.sort_date){
            let sort_date_start = req.query.sort_date+"T00:00:00.000Z"
            let sort_date_end = req.query.sort_date+"T23:59:59.000Z"
            users = await User.find({ 
                createdAt: { 
                    $gte: new Date(sort_date_start),
                    $lte: new Date(sort_date_end)
                } 
            });
        }else{
            users = await User.find()
        }

        res.status(200).json({
            status: 'success',
            data: {
                users
            }
        })
    }catch(err){
        res.status(404).json({
            status: 'fail',
            message: err
        })
    }
}

exports.getUser = async (req, res) => {
    try{
        const user = await User.findById(req.params.id)
        res.status(200).json({
            status: 'success',
            data: {
                user
            }
        })
    }catch(err){
        res.status(404).json({
            status: 'fail',
            message: err
        })
    }
}

exports.createUser = async (req, res) => {
    try{
        const newUser = await User.create(req.body)
        res.status(201).json({
            status: 'success',
            data: {
                newUser
            }
        })
    }catch(err){
        res.status(400).json({
            status: 'fail',
            message: err
        })
    }
}

exports.updateUser = async (req, res) => {
    try{
        const user = await User.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true
        })
        res.status(200).json({
            status: 'success',
            data: {
                user
            }
        })
    }catch(err){
        res.status(400).json({
            status: 'fail',
            message: err
        })
    }
}

exports.deleteUser = async (req, res) => {
    try{
        await User.findByIdAndDelete(req.params.id)
        res.status(204).json({
            status: 'success',
            data: null
        })
    }catch(err){
        res.status(400).json({
            status: 'fail',
            message: err
        })
    }
}

exports.getTimestamp = async (req, res) => {
    try{
        console.log(req.body)
        // await User.findByIdAndDelete(req.params.id)

        let startDate = req.body.startDate+"T00:00:00.000Z";
        let endDate = req.body.endDate+"T23:59:59.000Z";

        const tour = await Tour.find({
            "createdAt" : { 
                $gte : new Date(startDate).toISOString(),
                $lte: new Date(endDate).toISOString()
            }
        });

        // res.status(200).json({
        //     status: 'success',
        //     data: null
        // })
    }catch(err){
        res.status(400).json({
            status: 'fail',
            data: err
        })
    }
}