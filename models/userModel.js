const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'A user must have a name']
    },
    email: {
        type: String,
        required: [true, 'A user must have a email'],
        unique: true,
        lowercase: true
    },
    password: {
        type: String,
        required: [true, 'A user must have a password'],
    }
}, { timestamps: true })

const User = mongoose.model('User', userSchema)

module.exports = User