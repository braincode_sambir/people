const mongoose = require('mongoose')
try{
    mongoose.connect('mongodb://127.0.0.1:27017/people-app', {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
        useFindAndModify: false, 
    })
    console.log("DB connection was successful")

}catch(err){
    console.log(err)
}